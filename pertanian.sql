/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100116
Source Host           : localhost:3306
Source Database       : neracadb

Target Server Type    : MYSQL
Target Server Version : 100116
File Encoding         : 65001

Date: 2017-02-01 11:38:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bank
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bank` varchar(100) NOT NULL,
  `pemilik` varchar(100) NOT NULL,
  `no_rek` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank
-- ----------------------------
INSERT INTO `bank` VALUES ('1', 'BNI', '', '12345678', 'bni.png');
INSERT INTO `bank` VALUES ('2', 'BRI', '', '87873412323', 'bri.png');
INSERT INTO `bank` VALUES ('3', 'Mandiri', '', '778734098', 'mandiri.png');
INSERT INTO `bank` VALUES ('4', 'BCA', '', '998980342487', 'bca.png');

-- ----------------------------
-- Table structure for berita
-- ----------------------------
DROP TABLE IF EXISTS `berita`;
CREATE TABLE `berita` (
  `berita_id` int(11) NOT NULL AUTO_INCREMENT,
  `berita_kategori_id` int(11) DEFAULT NULL,
  `judul` varchar(100) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `isi` longtext NOT NULL,
  `tanggal` datetime NOT NULL,
  `jenis` enum('berita','halaman') NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`berita_id`),
  KEY `berita_kategori_id` (`berita_kategori_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of berita
-- ----------------------------
INSERT INTO `berita` VALUES ('4', null, 'Tentang Kami', 'tentang-kami', '<p>Tentang Kami</p>\r\n', '2016-05-13 14:54:24', 'halaman', 'publish', '4');
INSERT INTO `berita` VALUES ('5', null, 'Kontak Kami', 'kontak-kami', '<p>Kontak Kami</p>\r\n', '2016-05-13 14:54:33', 'halaman', 'publish', '4');
INSERT INTO `berita` VALUES ('6', null, 'Cara Pembelian', 'cara-pembelian', '<p>aaaaa</p>\r\n', '2016-05-13 14:54:49', 'halaman', 'publish', '4');
INSERT INTO `berita` VALUES ('7', null, 'Konfirmasi Pembayaran', 'konfirmasi-pembayaran', '<p>Konfirmasi Pembayaran</p>\r\n', '2016-05-13 14:55:07', 'halaman', 'publish', '4');

-- ----------------------------
-- Table structure for berita_kategori
-- ----------------------------
DROP TABLE IF EXISTS `berita_kategori`;
CREATE TABLE `berita_kategori` (
  `berita_kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(100) NOT NULL,
  `slug` varchar(200) NOT NULL,
  PRIMARY KEY (`berita_kategori_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of berita_kategori
-- ----------------------------

-- ----------------------------
-- Table structure for konfigurasi
-- ----------------------------
DROP TABLE IF EXISTS `konfigurasi`;
CREATE TABLE `konfigurasi` (
  `konfigurasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_key` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `tipe` varchar(15) NOT NULL,
  PRIMARY KEY (`konfigurasi_id`),
  UNIQUE KEY `nama_key` (`nama_key`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfigurasi
-- ----------------------------
INSERT INTO `konfigurasi` VALUES ('1', 'nama-aplikasi', '', 'umum');
INSERT INTO `konfigurasi` VALUES ('2', 'company-name', '', 'umum');
INSERT INTO `konfigurasi` VALUES ('3', 'company-address', '', 'umum');
INSERT INTO `konfigurasi` VALUES ('4', 'company-phone', '', 'umum');
INSERT INTO `konfigurasi` VALUES ('5', 'company-email', '', 'umum');
INSERT INTO `konfigurasi` VALUES ('6', 'tema-aktif', 'lte', 'tema');
INSERT INTO `konfigurasi` VALUES ('7', 'tema-logo', 'logo-c4ca4238a0b923820dcc509a6f75849b.jpg', 'tema');

-- ----------------------------
-- Table structure for lokasi_kota
-- ----------------------------
DROP TABLE IF EXISTS `lokasi_kota`;
CREATE TABLE `lokasi_kota` (
  `kota_id` int(11) NOT NULL AUTO_INCREMENT,
  `provinsi_id` int(11) NOT NULL,
  `nama_kota` varchar(100) NOT NULL,
  PRIMARY KEY (`kota_id`)
) ENGINE=MyISAM AUTO_INCREMENT=502 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lokasi_kota
-- ----------------------------
INSERT INTO `lokasi_kota` VALUES ('17', '1', 'Badung');
INSERT INTO `lokasi_kota` VALUES ('32', '1', 'Bangli');
INSERT INTO `lokasi_kota` VALUES ('94', '1', 'Buleleng');
INSERT INTO `lokasi_kota` VALUES ('114', '1', 'Denpasar');
INSERT INTO `lokasi_kota` VALUES ('128', '1', 'Gianyar');
INSERT INTO `lokasi_kota` VALUES ('161', '1', 'Jembrana');
INSERT INTO `lokasi_kota` VALUES ('170', '1', 'Karangasem');
INSERT INTO `lokasi_kota` VALUES ('197', '1', 'Klungkung');
INSERT INTO `lokasi_kota` VALUES ('447', '1', 'Tabanan');
INSERT INTO `lokasi_kota` VALUES ('27', '2', 'Bangka');
INSERT INTO `lokasi_kota` VALUES ('28', '2', 'Bangka Barat');
INSERT INTO `lokasi_kota` VALUES ('29', '2', 'Bangka Selatan');
INSERT INTO `lokasi_kota` VALUES ('30', '2', 'Bangka Tengah');
INSERT INTO `lokasi_kota` VALUES ('56', '2', 'Belitung');
INSERT INTO `lokasi_kota` VALUES ('57', '2', 'Belitung Timur');
INSERT INTO `lokasi_kota` VALUES ('334', '2', 'Pangkal Pinang');
INSERT INTO `lokasi_kota` VALUES ('106', '3', 'Cilegon');
INSERT INTO `lokasi_kota` VALUES ('232', '3', 'Lebak');
INSERT INTO `lokasi_kota` VALUES ('331', '3', 'Pandeglang');
INSERT INTO `lokasi_kota` VALUES ('402', '3', 'Serang');
INSERT INTO `lokasi_kota` VALUES ('403', '3', 'Serang');
INSERT INTO `lokasi_kota` VALUES ('455', '3', 'Tangerang');
INSERT INTO `lokasi_kota` VALUES ('456', '3', 'Tangerang');
INSERT INTO `lokasi_kota` VALUES ('457', '3', 'Tangerang Selatan');
INSERT INTO `lokasi_kota` VALUES ('62', '4', 'Bengkulu');
INSERT INTO `lokasi_kota` VALUES ('63', '4', 'Bengkulu Selatan');
INSERT INTO `lokasi_kota` VALUES ('64', '4', 'Bengkulu Tengah');
INSERT INTO `lokasi_kota` VALUES ('65', '4', 'Bengkulu Utara');
INSERT INTO `lokasi_kota` VALUES ('175', '4', 'Kaur');
INSERT INTO `lokasi_kota` VALUES ('183', '4', 'Kepahiang');
INSERT INTO `lokasi_kota` VALUES ('233', '4', 'Lebong');
INSERT INTO `lokasi_kota` VALUES ('294', '4', 'Muko Muko');
INSERT INTO `lokasi_kota` VALUES ('379', '4', 'Rejang Lebong');
INSERT INTO `lokasi_kota` VALUES ('397', '4', 'Seluma');
INSERT INTO `lokasi_kota` VALUES ('39', '5', 'Bantul');
INSERT INTO `lokasi_kota` VALUES ('135', '5', 'Gunung Kidul');
INSERT INTO `lokasi_kota` VALUES ('210', '5', 'Kulon Progo');
INSERT INTO `lokasi_kota` VALUES ('419', '5', 'Sleman');
INSERT INTO `lokasi_kota` VALUES ('501', '5', 'Yogyakarta');
INSERT INTO `lokasi_kota` VALUES ('151', '6', 'Jakarta Barat');
INSERT INTO `lokasi_kota` VALUES ('152', '6', 'Jakarta Pusat');
INSERT INTO `lokasi_kota` VALUES ('153', '6', 'Jakarta Selatan');
INSERT INTO `lokasi_kota` VALUES ('154', '6', 'Jakarta Timur');
INSERT INTO `lokasi_kota` VALUES ('155', '6', 'Jakarta Utara');
INSERT INTO `lokasi_kota` VALUES ('189', '6', 'Kepulauan Seribu');
INSERT INTO `lokasi_kota` VALUES ('77', '7', 'Boalemo');
INSERT INTO `lokasi_kota` VALUES ('88', '7', 'Bone Bolango');
INSERT INTO `lokasi_kota` VALUES ('129', '7', 'Gorontalo');
INSERT INTO `lokasi_kota` VALUES ('130', '7', 'Gorontalo');
INSERT INTO `lokasi_kota` VALUES ('131', '7', 'Gorontalo Utara');
INSERT INTO `lokasi_kota` VALUES ('361', '7', 'Pohuwato');
INSERT INTO `lokasi_kota` VALUES ('50', '8', 'Batang Hari');
INSERT INTO `lokasi_kota` VALUES ('97', '8', 'Bungo');
INSERT INTO `lokasi_kota` VALUES ('156', '8', 'Jambi');
INSERT INTO `lokasi_kota` VALUES ('194', '8', 'Kerinci');
INSERT INTO `lokasi_kota` VALUES ('280', '8', 'Merangin');
INSERT INTO `lokasi_kota` VALUES ('293', '8', 'Muaro Jambi');
INSERT INTO `lokasi_kota` VALUES ('393', '8', 'Sarolangun');
INSERT INTO `lokasi_kota` VALUES ('442', '8', 'Sungaipenuh');
INSERT INTO `lokasi_kota` VALUES ('460', '8', 'Tanjung Jabung Barat');
INSERT INTO `lokasi_kota` VALUES ('461', '8', 'Tanjung Jabung Timur');
INSERT INTO `lokasi_kota` VALUES ('471', '8', 'Tebo');
INSERT INTO `lokasi_kota` VALUES ('22', '9', 'Bandung');
INSERT INTO `lokasi_kota` VALUES ('23', '9', 'Bandung');
INSERT INTO `lokasi_kota` VALUES ('24', '9', 'Bandung Barat');
INSERT INTO `lokasi_kota` VALUES ('34', '9', 'Banjar');
INSERT INTO `lokasi_kota` VALUES ('54', '9', 'Bekasi');
INSERT INTO `lokasi_kota` VALUES ('55', '9', 'Bekasi');
INSERT INTO `lokasi_kota` VALUES ('78', '9', 'Bogor');
INSERT INTO `lokasi_kota` VALUES ('79', '9', 'Bogor');
INSERT INTO `lokasi_kota` VALUES ('103', '9', 'Ciamis');
INSERT INTO `lokasi_kota` VALUES ('104', '9', 'Cianjur');
INSERT INTO `lokasi_kota` VALUES ('107', '9', 'Cimahi');
INSERT INTO `lokasi_kota` VALUES ('108', '9', 'Cirebon');
INSERT INTO `lokasi_kota` VALUES ('109', '9', 'Cirebon');
INSERT INTO `lokasi_kota` VALUES ('115', '9', 'Depok');
INSERT INTO `lokasi_kota` VALUES ('126', '9', 'Garut');
INSERT INTO `lokasi_kota` VALUES ('149', '9', 'Indramayu');
INSERT INTO `lokasi_kota` VALUES ('171', '9', 'Karawang');
INSERT INTO `lokasi_kota` VALUES ('211', '9', 'Kuningan');
INSERT INTO `lokasi_kota` VALUES ('252', '9', 'Majalengka');
INSERT INTO `lokasi_kota` VALUES ('332', '9', 'Pangandaran');
INSERT INTO `lokasi_kota` VALUES ('376', '9', 'Purwakarta');
INSERT INTO `lokasi_kota` VALUES ('428', '9', 'Subang');
INSERT INTO `lokasi_kota` VALUES ('430', '9', 'Sukabumi');
INSERT INTO `lokasi_kota` VALUES ('431', '9', 'Sukabumi');
INSERT INTO `lokasi_kota` VALUES ('440', '9', 'Sumedang');
INSERT INTO `lokasi_kota` VALUES ('468', '9', 'Tasikmalaya');
INSERT INTO `lokasi_kota` VALUES ('469', '9', 'Tasikmalaya');
INSERT INTO `lokasi_kota` VALUES ('37', '10', 'Banjarnegara');
INSERT INTO `lokasi_kota` VALUES ('41', '10', 'Banyumas');
INSERT INTO `lokasi_kota` VALUES ('49', '10', 'Batang');
INSERT INTO `lokasi_kota` VALUES ('76', '10', 'Blora');
INSERT INTO `lokasi_kota` VALUES ('91', '10', 'Boyolali');
INSERT INTO `lokasi_kota` VALUES ('92', '10', 'Brebes');
INSERT INTO `lokasi_kota` VALUES ('105', '10', 'Cilacap');
INSERT INTO `lokasi_kota` VALUES ('113', '10', 'Demak');
INSERT INTO `lokasi_kota` VALUES ('134', '10', 'Grobogan');
INSERT INTO `lokasi_kota` VALUES ('163', '10', 'Jepara');
INSERT INTO `lokasi_kota` VALUES ('169', '10', 'Karanganyar');
INSERT INTO `lokasi_kota` VALUES ('177', '10', 'Kebumen');
INSERT INTO `lokasi_kota` VALUES ('181', '10', 'Kendal');
INSERT INTO `lokasi_kota` VALUES ('196', '10', 'Klaten');
INSERT INTO `lokasi_kota` VALUES ('209', '10', 'Kudus');
INSERT INTO `lokasi_kota` VALUES ('249', '10', 'Magelang');
INSERT INTO `lokasi_kota` VALUES ('250', '10', 'Magelang');
INSERT INTO `lokasi_kota` VALUES ('344', '10', 'Pati');
INSERT INTO `lokasi_kota` VALUES ('348', '10', 'Pekalongan');
INSERT INTO `lokasi_kota` VALUES ('349', '10', 'Pekalongan');
INSERT INTO `lokasi_kota` VALUES ('352', '10', 'Pemalang');
INSERT INTO `lokasi_kota` VALUES ('375', '10', 'Purbalingga');
INSERT INTO `lokasi_kota` VALUES ('377', '10', 'Purworejo');
INSERT INTO `lokasi_kota` VALUES ('380', '10', 'Rembang');
INSERT INTO `lokasi_kota` VALUES ('386', '10', 'Salatiga');
INSERT INTO `lokasi_kota` VALUES ('398', '10', 'Semarang');
INSERT INTO `lokasi_kota` VALUES ('399', '10', 'Semarang');
INSERT INTO `lokasi_kota` VALUES ('427', '10', 'Sragen');
INSERT INTO `lokasi_kota` VALUES ('433', '10', 'Sukoharjo');
INSERT INTO `lokasi_kota` VALUES ('445', '10', 'Surakarta (Solo)');
INSERT INTO `lokasi_kota` VALUES ('472', '10', 'Tegal');
INSERT INTO `lokasi_kota` VALUES ('473', '10', 'Tegal');
INSERT INTO `lokasi_kota` VALUES ('476', '10', 'Temanggung');
INSERT INTO `lokasi_kota` VALUES ('497', '10', 'Wonogiri');
INSERT INTO `lokasi_kota` VALUES ('498', '10', 'Wonosobo');
INSERT INTO `lokasi_kota` VALUES ('31', '11', 'Bangkalan');
INSERT INTO `lokasi_kota` VALUES ('42', '11', 'Banyuwangi');
INSERT INTO `lokasi_kota` VALUES ('51', '11', 'Batu');
INSERT INTO `lokasi_kota` VALUES ('74', '11', 'Blitar');
INSERT INTO `lokasi_kota` VALUES ('75', '11', 'Blitar');
INSERT INTO `lokasi_kota` VALUES ('80', '11', 'Bojonegoro');
INSERT INTO `lokasi_kota` VALUES ('86', '11', 'Bondowoso');
INSERT INTO `lokasi_kota` VALUES ('133', '11', 'Gresik');
INSERT INTO `lokasi_kota` VALUES ('160', '11', 'Jember');
INSERT INTO `lokasi_kota` VALUES ('164', '11', 'Jombang');
INSERT INTO `lokasi_kota` VALUES ('178', '11', 'Kediri');
INSERT INTO `lokasi_kota` VALUES ('179', '11', 'Kediri');
INSERT INTO `lokasi_kota` VALUES ('222', '11', 'Lamongan');
INSERT INTO `lokasi_kota` VALUES ('243', '11', 'Lumajang');
INSERT INTO `lokasi_kota` VALUES ('247', '11', 'Madiun');
INSERT INTO `lokasi_kota` VALUES ('248', '11', 'Kab. Madiun');
INSERT INTO `lokasi_kota` VALUES ('251', '11', 'Magetan');
INSERT INTO `lokasi_kota` VALUES ('256', '11', 'Malang');
INSERT INTO `lokasi_kota` VALUES ('255', '11', 'Kab. Malang');
INSERT INTO `lokasi_kota` VALUES ('289', '11', 'Mojokerto');
INSERT INTO `lokasi_kota` VALUES ('290', '11', 'Kab. Mojokerto');
INSERT INTO `lokasi_kota` VALUES ('305', '11', 'Nganjuk');
INSERT INTO `lokasi_kota` VALUES ('306', '11', 'Ngawi');
INSERT INTO `lokasi_kota` VALUES ('317', '11', 'Pacitan');
INSERT INTO `lokasi_kota` VALUES ('330', '11', 'Pamekasan');
INSERT INTO `lokasi_kota` VALUES ('342', '11', 'Pasuruan');
INSERT INTO `lokasi_kota` VALUES ('343', '11', 'Pasuruan');
INSERT INTO `lokasi_kota` VALUES ('363', '11', 'Ponorogo');
INSERT INTO `lokasi_kota` VALUES ('369', '11', 'Kab. Probolinggo');
INSERT INTO `lokasi_kota` VALUES ('370', '11', 'Probolinggo');
INSERT INTO `lokasi_kota` VALUES ('390', '11', 'Sampang');
INSERT INTO `lokasi_kota` VALUES ('409', '11', 'Sidoarjo');
INSERT INTO `lokasi_kota` VALUES ('418', '11', 'Situbondo');
INSERT INTO `lokasi_kota` VALUES ('441', '11', 'Sumenep');
INSERT INTO `lokasi_kota` VALUES ('444', '11', 'Surabaya');
INSERT INTO `lokasi_kota` VALUES ('487', '11', 'Trenggalek');
INSERT INTO `lokasi_kota` VALUES ('489', '11', 'Tuban');
INSERT INTO `lokasi_kota` VALUES ('492', '11', 'Tulungagung');
INSERT INTO `lokasi_kota` VALUES ('61', '12', 'Bengkayang');
INSERT INTO `lokasi_kota` VALUES ('168', '12', 'Kapuas Hulu');
INSERT INTO `lokasi_kota` VALUES ('176', '12', 'Kayong Utara');
INSERT INTO `lokasi_kota` VALUES ('195', '12', 'Ketapang');
INSERT INTO `lokasi_kota` VALUES ('208', '12', 'Kubu Raya');
INSERT INTO `lokasi_kota` VALUES ('228', '12', 'Landak');
INSERT INTO `lokasi_kota` VALUES ('279', '12', 'Melawi');
INSERT INTO `lokasi_kota` VALUES ('364', '12', 'Pontianak');
INSERT INTO `lokasi_kota` VALUES ('365', '12', 'Pontianak');
INSERT INTO `lokasi_kota` VALUES ('388', '12', 'Sambas');
INSERT INTO `lokasi_kota` VALUES ('391', '12', 'Sanggau');
INSERT INTO `lokasi_kota` VALUES ('395', '12', 'Sekadau');
INSERT INTO `lokasi_kota` VALUES ('415', '12', 'Singkawang');
INSERT INTO `lokasi_kota` VALUES ('417', '12', 'Sintang');
INSERT INTO `lokasi_kota` VALUES ('18', '13', 'Balangan');
INSERT INTO `lokasi_kota` VALUES ('33', '13', 'Banjar');
INSERT INTO `lokasi_kota` VALUES ('35', '13', 'Banjarbaru');
INSERT INTO `lokasi_kota` VALUES ('36', '13', 'Banjarmasin');
INSERT INTO `lokasi_kota` VALUES ('43', '13', 'Barito Kuala');
INSERT INTO `lokasi_kota` VALUES ('143', '13', 'Hulu Sungai Selatan');
INSERT INTO `lokasi_kota` VALUES ('144', '13', 'Hulu Sungai Tengah');
INSERT INTO `lokasi_kota` VALUES ('145', '13', 'Hulu Sungai Utara');
INSERT INTO `lokasi_kota` VALUES ('203', '13', 'Kotabaru');
INSERT INTO `lokasi_kota` VALUES ('446', '13', 'Tabalong');
INSERT INTO `lokasi_kota` VALUES ('452', '13', 'Tanah Bumbu');
INSERT INTO `lokasi_kota` VALUES ('454', '13', 'Tanah Laut');
INSERT INTO `lokasi_kota` VALUES ('466', '13', 'Tapin');
INSERT INTO `lokasi_kota` VALUES ('44', '14', 'Barito Selatan');
INSERT INTO `lokasi_kota` VALUES ('45', '14', 'Barito Timur');
INSERT INTO `lokasi_kota` VALUES ('46', '14', 'Barito Utara');
INSERT INTO `lokasi_kota` VALUES ('136', '14', 'Gunung Mas');
INSERT INTO `lokasi_kota` VALUES ('167', '14', 'Kapuas');
INSERT INTO `lokasi_kota` VALUES ('174', '14', 'Katingan');
INSERT INTO `lokasi_kota` VALUES ('205', '14', 'Kotawaringin Barat');
INSERT INTO `lokasi_kota` VALUES ('206', '14', 'Kotawaringin Timur');
INSERT INTO `lokasi_kota` VALUES ('221', '14', 'Lamandau');
INSERT INTO `lokasi_kota` VALUES ('296', '14', 'Murung Raya');
INSERT INTO `lokasi_kota` VALUES ('326', '14', 'Palangka Raya');
INSERT INTO `lokasi_kota` VALUES ('371', '14', 'Pulang Pisau');
INSERT INTO `lokasi_kota` VALUES ('405', '14', 'Seruyan');
INSERT INTO `lokasi_kota` VALUES ('432', '14', 'Sukamara');
INSERT INTO `lokasi_kota` VALUES ('19', '15', 'Balikpapan');
INSERT INTO `lokasi_kota` VALUES ('66', '15', 'Berau');
INSERT INTO `lokasi_kota` VALUES ('89', '15', 'Bontang');
INSERT INTO `lokasi_kota` VALUES ('214', '15', 'Kutai Barat');
INSERT INTO `lokasi_kota` VALUES ('215', '15', 'Kutai Kartanegara');
INSERT INTO `lokasi_kota` VALUES ('216', '15', 'Kutai Timur');
INSERT INTO `lokasi_kota` VALUES ('341', '15', 'Paser');
INSERT INTO `lokasi_kota` VALUES ('354', '15', 'Penajam Paser Utara');
INSERT INTO `lokasi_kota` VALUES ('387', '15', 'Samarinda');
INSERT INTO `lokasi_kota` VALUES ('96', '16', 'Bulungan (Bulongan)');
INSERT INTO `lokasi_kota` VALUES ('257', '16', 'Malinau');
INSERT INTO `lokasi_kota` VALUES ('311', '16', 'Nunukan');
INSERT INTO `lokasi_kota` VALUES ('450', '16', 'Tana Tidung');
INSERT INTO `lokasi_kota` VALUES ('467', '16', 'Tarakan');
INSERT INTO `lokasi_kota` VALUES ('48', '17', 'Batam');
INSERT INTO `lokasi_kota` VALUES ('71', '17', 'Bintan');
INSERT INTO `lokasi_kota` VALUES ('172', '17', 'Karimun');
INSERT INTO `lokasi_kota` VALUES ('184', '17', 'Kepulauan Anambas');
INSERT INTO `lokasi_kota` VALUES ('237', '17', 'Lingga');
INSERT INTO `lokasi_kota` VALUES ('302', '17', 'Natuna');
INSERT INTO `lokasi_kota` VALUES ('462', '17', 'Tanjung Pinang');
INSERT INTO `lokasi_kota` VALUES ('21', '18', 'Bandar Lampung');
INSERT INTO `lokasi_kota` VALUES ('223', '18', 'Lampung Barat');
INSERT INTO `lokasi_kota` VALUES ('224', '18', 'Lampung Selatan');
INSERT INTO `lokasi_kota` VALUES ('225', '18', 'Lampung Tengah');
INSERT INTO `lokasi_kota` VALUES ('226', '18', 'Lampung Timur');
INSERT INTO `lokasi_kota` VALUES ('227', '18', 'Lampung Utara');
INSERT INTO `lokasi_kota` VALUES ('282', '18', 'Mesuji');
INSERT INTO `lokasi_kota` VALUES ('283', '18', 'Metro');
INSERT INTO `lokasi_kota` VALUES ('355', '18', 'Pesawaran');
INSERT INTO `lokasi_kota` VALUES ('356', '18', 'Pesisir Barat');
INSERT INTO `lokasi_kota` VALUES ('368', '18', 'Pringsewu');
INSERT INTO `lokasi_kota` VALUES ('458', '18', 'Tanggamus');
INSERT INTO `lokasi_kota` VALUES ('490', '18', 'Tulang Bawang');
INSERT INTO `lokasi_kota` VALUES ('491', '18', 'Tulang Bawang Barat');
INSERT INTO `lokasi_kota` VALUES ('496', '18', 'Way Kanan');
INSERT INTO `lokasi_kota` VALUES ('14', '19', 'Ambon');
INSERT INTO `lokasi_kota` VALUES ('99', '19', 'Buru');
INSERT INTO `lokasi_kota` VALUES ('100', '19', 'Buru Selatan');
INSERT INTO `lokasi_kota` VALUES ('185', '19', 'Kepulauan Aru');
INSERT INTO `lokasi_kota` VALUES ('258', '19', 'Maluku Barat Daya');
INSERT INTO `lokasi_kota` VALUES ('259', '19', 'Maluku Tengah');
INSERT INTO `lokasi_kota` VALUES ('260', '19', 'Maluku Tenggara');
INSERT INTO `lokasi_kota` VALUES ('261', '19', 'Maluku Tenggara Barat');
INSERT INTO `lokasi_kota` VALUES ('400', '19', 'Seram Bagian Barat');
INSERT INTO `lokasi_kota` VALUES ('401', '19', 'Seram Bagian Timur');
INSERT INTO `lokasi_kota` VALUES ('488', '19', 'Tual');
INSERT INTO `lokasi_kota` VALUES ('138', '20', 'Halmahera Barat');
INSERT INTO `lokasi_kota` VALUES ('139', '20', 'Halmahera Selatan');
INSERT INTO `lokasi_kota` VALUES ('140', '20', 'Halmahera Tengah');
INSERT INTO `lokasi_kota` VALUES ('141', '20', 'Halmahera Timur');
INSERT INTO `lokasi_kota` VALUES ('142', '20', 'Halmahera Utara');
INSERT INTO `lokasi_kota` VALUES ('191', '20', 'Kepulauan Sula');
INSERT INTO `lokasi_kota` VALUES ('372', '20', 'Pulau Morotai');
INSERT INTO `lokasi_kota` VALUES ('477', '20', 'Ternate');
INSERT INTO `lokasi_kota` VALUES ('478', '20', 'Tidore Kepulauan');
INSERT INTO `lokasi_kota` VALUES ('1', '21', 'Aceh Barat');
INSERT INTO `lokasi_kota` VALUES ('2', '21', 'Aceh Barat Daya');
INSERT INTO `lokasi_kota` VALUES ('3', '21', 'Aceh Besar');
INSERT INTO `lokasi_kota` VALUES ('4', '21', 'Aceh Jaya');
INSERT INTO `lokasi_kota` VALUES ('5', '21', 'Aceh Selatan');
INSERT INTO `lokasi_kota` VALUES ('6', '21', 'Aceh Singkil');
INSERT INTO `lokasi_kota` VALUES ('7', '21', 'Aceh Tamiang');
INSERT INTO `lokasi_kota` VALUES ('8', '21', 'Aceh Tengah');
INSERT INTO `lokasi_kota` VALUES ('9', '21', 'Aceh Tenggara');
INSERT INTO `lokasi_kota` VALUES ('10', '21', 'Aceh Timur');
INSERT INTO `lokasi_kota` VALUES ('11', '21', 'Aceh Utara');
INSERT INTO `lokasi_kota` VALUES ('20', '21', 'Banda Aceh');
INSERT INTO `lokasi_kota` VALUES ('59', '21', 'Bener Meriah');
INSERT INTO `lokasi_kota` VALUES ('72', '21', 'Bireuen');
INSERT INTO `lokasi_kota` VALUES ('127', '21', 'Gayo Lues');
INSERT INTO `lokasi_kota` VALUES ('230', '21', 'Langsa');
INSERT INTO `lokasi_kota` VALUES ('235', '21', 'Lhokseumawe');
INSERT INTO `lokasi_kota` VALUES ('300', '21', 'Nagan Raya');
INSERT INTO `lokasi_kota` VALUES ('358', '21', 'Pidie');
INSERT INTO `lokasi_kota` VALUES ('359', '21', 'Pidie Jaya');
INSERT INTO `lokasi_kota` VALUES ('384', '21', 'Sabang');
INSERT INTO `lokasi_kota` VALUES ('414', '21', 'Simeulue');
INSERT INTO `lokasi_kota` VALUES ('429', '21', 'Subulussalam');
INSERT INTO `lokasi_kota` VALUES ('68', '22', 'Bima');
INSERT INTO `lokasi_kota` VALUES ('69', '22', 'Bima');
INSERT INTO `lokasi_kota` VALUES ('118', '22', 'Dompu');
INSERT INTO `lokasi_kota` VALUES ('238', '22', 'Lombok Barat');
INSERT INTO `lokasi_kota` VALUES ('239', '22', 'Lombok Tengah');
INSERT INTO `lokasi_kota` VALUES ('240', '22', 'Lombok Timur');
INSERT INTO `lokasi_kota` VALUES ('241', '22', 'Lombok Utara');
INSERT INTO `lokasi_kota` VALUES ('276', '22', 'Mataram');
INSERT INTO `lokasi_kota` VALUES ('438', '22', 'Sumbawa');
INSERT INTO `lokasi_kota` VALUES ('439', '22', 'Sumbawa Barat');
INSERT INTO `lokasi_kota` VALUES ('13', '23', 'Alor');
INSERT INTO `lokasi_kota` VALUES ('58', '23', 'Belu');
INSERT INTO `lokasi_kota` VALUES ('122', '23', 'Ende');
INSERT INTO `lokasi_kota` VALUES ('125', '23', 'Flores Timur');
INSERT INTO `lokasi_kota` VALUES ('212', '23', 'Kupang');
INSERT INTO `lokasi_kota` VALUES ('213', '23', 'Kupang');
INSERT INTO `lokasi_kota` VALUES ('234', '23', 'Lembata');
INSERT INTO `lokasi_kota` VALUES ('269', '23', 'Manggarai');
INSERT INTO `lokasi_kota` VALUES ('270', '23', 'Manggarai Barat');
INSERT INTO `lokasi_kota` VALUES ('271', '23', 'Manggarai Timur');
INSERT INTO `lokasi_kota` VALUES ('301', '23', 'Nagekeo');
INSERT INTO `lokasi_kota` VALUES ('304', '23', 'Ngada');
INSERT INTO `lokasi_kota` VALUES ('383', '23', 'Rote Ndao');
INSERT INTO `lokasi_kota` VALUES ('385', '23', 'Sabu Raijua');
INSERT INTO `lokasi_kota` VALUES ('412', '23', 'Sikka');
INSERT INTO `lokasi_kota` VALUES ('434', '23', 'Sumba Barat');
INSERT INTO `lokasi_kota` VALUES ('435', '23', 'Sumba Barat Daya');
INSERT INTO `lokasi_kota` VALUES ('436', '23', 'Sumba Tengah');
INSERT INTO `lokasi_kota` VALUES ('437', '23', 'Sumba Timur');
INSERT INTO `lokasi_kota` VALUES ('479', '23', 'Timor Tengah Selatan');
INSERT INTO `lokasi_kota` VALUES ('480', '23', 'Timor Tengah Utara');
INSERT INTO `lokasi_kota` VALUES ('16', '24', 'Asmat');
INSERT INTO `lokasi_kota` VALUES ('67', '24', 'Biak Numfor');
INSERT INTO `lokasi_kota` VALUES ('90', '24', 'Boven Digoel');
INSERT INTO `lokasi_kota` VALUES ('111', '24', 'Deiyai (Deliyai)');
INSERT INTO `lokasi_kota` VALUES ('117', '24', 'Dogiyai');
INSERT INTO `lokasi_kota` VALUES ('150', '24', 'Intan Jaya');
INSERT INTO `lokasi_kota` VALUES ('157', '24', 'Jayapura');
INSERT INTO `lokasi_kota` VALUES ('158', '24', 'Jayapura');
INSERT INTO `lokasi_kota` VALUES ('159', '24', 'Jayawijaya');
INSERT INTO `lokasi_kota` VALUES ('180', '24', 'Keerom');
INSERT INTO `lokasi_kota` VALUES ('193', '24', 'Kepulauan Yapen (Yapen Waropen)');
INSERT INTO `lokasi_kota` VALUES ('231', '24', 'Lanny Jaya');
INSERT INTO `lokasi_kota` VALUES ('263', '24', 'Mamberamo Raya');
INSERT INTO `lokasi_kota` VALUES ('264', '24', 'Mamberamo Tengah');
INSERT INTO `lokasi_kota` VALUES ('274', '24', 'Mappi');
INSERT INTO `lokasi_kota` VALUES ('281', '24', 'Merauke');
INSERT INTO `lokasi_kota` VALUES ('284', '24', 'Mimika');
INSERT INTO `lokasi_kota` VALUES ('299', '24', 'Nabire');
INSERT INTO `lokasi_kota` VALUES ('303', '24', 'Nduga');
INSERT INTO `lokasi_kota` VALUES ('335', '24', 'Paniai');
INSERT INTO `lokasi_kota` VALUES ('347', '24', 'Pegunungan Bintang');
INSERT INTO `lokasi_kota` VALUES ('373', '24', 'Puncak');
INSERT INTO `lokasi_kota` VALUES ('374', '24', 'Puncak Jaya');
INSERT INTO `lokasi_kota` VALUES ('392', '24', 'Sarmi');
INSERT INTO `lokasi_kota` VALUES ('443', '24', 'Supiori');
INSERT INTO `lokasi_kota` VALUES ('484', '24', 'Tolikara');
INSERT INTO `lokasi_kota` VALUES ('495', '24', 'Waropen');
INSERT INTO `lokasi_kota` VALUES ('499', '24', 'Yahukimo');
INSERT INTO `lokasi_kota` VALUES ('500', '24', 'Yalimo');
INSERT INTO `lokasi_kota` VALUES ('124', '25', 'Fakfak');
INSERT INTO `lokasi_kota` VALUES ('165', '25', 'Kaimana');
INSERT INTO `lokasi_kota` VALUES ('272', '25', 'Manokwari');
INSERT INTO `lokasi_kota` VALUES ('273', '25', 'Manokwari Selatan');
INSERT INTO `lokasi_kota` VALUES ('277', '25', 'Maybrat');
INSERT INTO `lokasi_kota` VALUES ('346', '25', 'Pegunungan Arfak');
INSERT INTO `lokasi_kota` VALUES ('378', '25', 'Raja Ampat');
INSERT INTO `lokasi_kota` VALUES ('424', '25', 'Sorong');
INSERT INTO `lokasi_kota` VALUES ('425', '25', 'Sorong');
INSERT INTO `lokasi_kota` VALUES ('426', '25', 'Sorong Selatan');
INSERT INTO `lokasi_kota` VALUES ('449', '25', 'Tambrauw');
INSERT INTO `lokasi_kota` VALUES ('474', '25', 'Teluk Bintuni');
INSERT INTO `lokasi_kota` VALUES ('475', '25', 'Teluk Wondama');
INSERT INTO `lokasi_kota` VALUES ('60', '26', 'Bengkalis');
INSERT INTO `lokasi_kota` VALUES ('120', '26', 'Dumai');
INSERT INTO `lokasi_kota` VALUES ('147', '26', 'Indragiri Hilir');
INSERT INTO `lokasi_kota` VALUES ('148', '26', 'Indragiri Hulu');
INSERT INTO `lokasi_kota` VALUES ('166', '26', 'Kampar');
INSERT INTO `lokasi_kota` VALUES ('187', '26', 'Kepulauan Meranti');
INSERT INTO `lokasi_kota` VALUES ('207', '26', 'Kuantan Singingi');
INSERT INTO `lokasi_kota` VALUES ('350', '26', 'Pekanbaru');
INSERT INTO `lokasi_kota` VALUES ('351', '26', 'Pelalawan');
INSERT INTO `lokasi_kota` VALUES ('381', '26', 'Rokan Hilir');
INSERT INTO `lokasi_kota` VALUES ('382', '26', 'Rokan Hulu');
INSERT INTO `lokasi_kota` VALUES ('406', '26', 'Siak');
INSERT INTO `lokasi_kota` VALUES ('253', '27', 'Majene');
INSERT INTO `lokasi_kota` VALUES ('262', '27', 'Mamasa');
INSERT INTO `lokasi_kota` VALUES ('265', '27', 'Mamuju');
INSERT INTO `lokasi_kota` VALUES ('266', '27', 'Mamuju Utara');
INSERT INTO `lokasi_kota` VALUES ('362', '27', 'Polewali Mandar');
INSERT INTO `lokasi_kota` VALUES ('38', '28', 'Bantaeng');
INSERT INTO `lokasi_kota` VALUES ('47', '28', 'Barru');
INSERT INTO `lokasi_kota` VALUES ('87', '28', 'Bone');
INSERT INTO `lokasi_kota` VALUES ('95', '28', 'Bulukumba');
INSERT INTO `lokasi_kota` VALUES ('123', '28', 'Enrekang');
INSERT INTO `lokasi_kota` VALUES ('132', '28', 'Gowa');
INSERT INTO `lokasi_kota` VALUES ('162', '28', 'Jeneponto');
INSERT INTO `lokasi_kota` VALUES ('244', '28', 'Luwu');
INSERT INTO `lokasi_kota` VALUES ('245', '28', 'Luwu Timur');
INSERT INTO `lokasi_kota` VALUES ('246', '28', 'Luwu Utara');
INSERT INTO `lokasi_kota` VALUES ('254', '28', 'Makassar');
INSERT INTO `lokasi_kota` VALUES ('275', '28', 'Maros');
INSERT INTO `lokasi_kota` VALUES ('328', '28', 'Palopo');
INSERT INTO `lokasi_kota` VALUES ('333', '28', 'Pangkajene Kepulauan');
INSERT INTO `lokasi_kota` VALUES ('336', '28', 'Parepare');
INSERT INTO `lokasi_kota` VALUES ('360', '28', 'Pinrang');
INSERT INTO `lokasi_kota` VALUES ('396', '28', 'Selayar (Kepulauan Selayar)');
INSERT INTO `lokasi_kota` VALUES ('408', '28', 'Sidenreng Rappang/Rapang');
INSERT INTO `lokasi_kota` VALUES ('416', '28', 'Sinjai');
INSERT INTO `lokasi_kota` VALUES ('423', '28', 'Soppeng');
INSERT INTO `lokasi_kota` VALUES ('448', '28', 'Takalar');
INSERT INTO `lokasi_kota` VALUES ('451', '28', 'Tana Toraja');
INSERT INTO `lokasi_kota` VALUES ('486', '28', 'Toraja Utara');
INSERT INTO `lokasi_kota` VALUES ('493', '28', 'Wajo');
INSERT INTO `lokasi_kota` VALUES ('25', '29', 'Banggai');
INSERT INTO `lokasi_kota` VALUES ('26', '29', 'Banggai Kepulauan');
INSERT INTO `lokasi_kota` VALUES ('98', '29', 'Buol');
INSERT INTO `lokasi_kota` VALUES ('119', '29', 'Donggala');
INSERT INTO `lokasi_kota` VALUES ('291', '29', 'Morowali');
INSERT INTO `lokasi_kota` VALUES ('329', '29', 'Palu');
INSERT INTO `lokasi_kota` VALUES ('338', '29', 'Parigi Moutong');
INSERT INTO `lokasi_kota` VALUES ('366', '29', 'Poso');
INSERT INTO `lokasi_kota` VALUES ('410', '29', 'Sigi');
INSERT INTO `lokasi_kota` VALUES ('482', '29', 'Tojo Una-Una');
INSERT INTO `lokasi_kota` VALUES ('483', '29', 'Toli-Toli');
INSERT INTO `lokasi_kota` VALUES ('53', '30', 'Bau-Bau');
INSERT INTO `lokasi_kota` VALUES ('85', '30', 'Bombana');
INSERT INTO `lokasi_kota` VALUES ('101', '30', 'Buton');
INSERT INTO `lokasi_kota` VALUES ('102', '30', 'Buton Utara');
INSERT INTO `lokasi_kota` VALUES ('182', '30', 'Kendari');
INSERT INTO `lokasi_kota` VALUES ('198', '30', 'Kolaka');
INSERT INTO `lokasi_kota` VALUES ('199', '30', 'Kolaka Utara');
INSERT INTO `lokasi_kota` VALUES ('200', '30', 'Konawe');
INSERT INTO `lokasi_kota` VALUES ('201', '30', 'Konawe Selatan');
INSERT INTO `lokasi_kota` VALUES ('202', '30', 'Konawe Utara');
INSERT INTO `lokasi_kota` VALUES ('295', '30', 'Muna');
INSERT INTO `lokasi_kota` VALUES ('494', '30', 'Wakatobi');
INSERT INTO `lokasi_kota` VALUES ('73', '31', 'Bitung');
INSERT INTO `lokasi_kota` VALUES ('81', '31', 'Bolaang Mongondow (Bolmong)');
INSERT INTO `lokasi_kota` VALUES ('82', '31', 'Bolaang Mongondow Selatan');
INSERT INTO `lokasi_kota` VALUES ('83', '31', 'Bolaang Mongondow Timur');
INSERT INTO `lokasi_kota` VALUES ('84', '31', 'Bolaang Mongondow Utara');
INSERT INTO `lokasi_kota` VALUES ('188', '31', 'Kepulauan Sangihe');
INSERT INTO `lokasi_kota` VALUES ('190', '31', 'Kepulauan Siau Tagulandang Biaro (Sitaro)');
INSERT INTO `lokasi_kota` VALUES ('192', '31', 'Kepulauan Talaud');
INSERT INTO `lokasi_kota` VALUES ('204', '31', 'Kotamobagu');
INSERT INTO `lokasi_kota` VALUES ('267', '31', 'Manado');
INSERT INTO `lokasi_kota` VALUES ('285', '31', 'Minahasa');
INSERT INTO `lokasi_kota` VALUES ('286', '31', 'Minahasa Selatan');
INSERT INTO `lokasi_kota` VALUES ('287', '31', 'Minahasa Tenggara');
INSERT INTO `lokasi_kota` VALUES ('288', '31', 'Minahasa Utara');
INSERT INTO `lokasi_kota` VALUES ('485', '31', 'Tomohon');
INSERT INTO `lokasi_kota` VALUES ('12', '32', 'Agam');
INSERT INTO `lokasi_kota` VALUES ('93', '32', 'Bukittinggi');
INSERT INTO `lokasi_kota` VALUES ('116', '32', 'Dharmasraya');
INSERT INTO `lokasi_kota` VALUES ('186', '32', 'Kepulauan Mentawai');
INSERT INTO `lokasi_kota` VALUES ('236', '32', 'Lima Puluh Koto/Kota');
INSERT INTO `lokasi_kota` VALUES ('318', '32', 'Padang');
INSERT INTO `lokasi_kota` VALUES ('321', '32', 'Padang Panjang');
INSERT INTO `lokasi_kota` VALUES ('322', '32', 'Padang Pariaman');
INSERT INTO `lokasi_kota` VALUES ('337', '32', 'Pariaman');
INSERT INTO `lokasi_kota` VALUES ('339', '32', 'Pasaman');
INSERT INTO `lokasi_kota` VALUES ('340', '32', 'Pasaman Barat');
INSERT INTO `lokasi_kota` VALUES ('345', '32', 'Payakumbuh');
INSERT INTO `lokasi_kota` VALUES ('357', '32', 'Pesisir Selatan');
INSERT INTO `lokasi_kota` VALUES ('394', '32', 'Sawah Lunto');
INSERT INTO `lokasi_kota` VALUES ('411', '32', 'Sijunjung (Sawah Lunto Sijunjung)');
INSERT INTO `lokasi_kota` VALUES ('420', '32', 'Solok');
INSERT INTO `lokasi_kota` VALUES ('421', '32', 'Solok');
INSERT INTO `lokasi_kota` VALUES ('422', '32', 'Solok Selatan');
INSERT INTO `lokasi_kota` VALUES ('453', '32', 'Tanah Datar');
INSERT INTO `lokasi_kota` VALUES ('40', '33', 'Banyuasin');
INSERT INTO `lokasi_kota` VALUES ('121', '33', 'Empat Lawang');
INSERT INTO `lokasi_kota` VALUES ('220', '33', 'Lahat');
INSERT INTO `lokasi_kota` VALUES ('242', '33', 'Lubuk Linggau');
INSERT INTO `lokasi_kota` VALUES ('292', '33', 'Muara Enim');
INSERT INTO `lokasi_kota` VALUES ('297', '33', 'Musi Banyuasin');
INSERT INTO `lokasi_kota` VALUES ('298', '33', 'Musi Rawas');
INSERT INTO `lokasi_kota` VALUES ('312', '33', 'Ogan Ilir');
INSERT INTO `lokasi_kota` VALUES ('313', '33', 'Ogan Komering Ilir');
INSERT INTO `lokasi_kota` VALUES ('314', '33', 'Ogan Komering Ulu');
INSERT INTO `lokasi_kota` VALUES ('315', '33', 'Ogan Komering Ulu Selatan');
INSERT INTO `lokasi_kota` VALUES ('316', '33', 'Ogan Komering Ulu Timur');
INSERT INTO `lokasi_kota` VALUES ('324', '33', 'Pagar Alam');
INSERT INTO `lokasi_kota` VALUES ('327', '33', 'Palembang');
INSERT INTO `lokasi_kota` VALUES ('367', '33', 'Prabumulih');
INSERT INTO `lokasi_kota` VALUES ('15', '34', 'Asahan');
INSERT INTO `lokasi_kota` VALUES ('52', '34', 'Batu Bara');
INSERT INTO `lokasi_kota` VALUES ('70', '34', 'Binjai');
INSERT INTO `lokasi_kota` VALUES ('110', '34', 'Dairi');
INSERT INTO `lokasi_kota` VALUES ('112', '34', 'Deli Serdang');
INSERT INTO `lokasi_kota` VALUES ('137', '34', 'Gunungsitoli');
INSERT INTO `lokasi_kota` VALUES ('146', '34', 'Humbang Hasundutan');
INSERT INTO `lokasi_kota` VALUES ('173', '34', 'Karo');
INSERT INTO `lokasi_kota` VALUES ('217', '34', 'Labuhan Batu');
INSERT INTO `lokasi_kota` VALUES ('218', '34', 'Labuhan Batu Selatan');
INSERT INTO `lokasi_kota` VALUES ('219', '34', 'Labuhan Batu Utara');
INSERT INTO `lokasi_kota` VALUES ('229', '34', 'Langkat');
INSERT INTO `lokasi_kota` VALUES ('268', '34', 'Mandailing Natal');
INSERT INTO `lokasi_kota` VALUES ('278', '34', 'Medan');
INSERT INTO `lokasi_kota` VALUES ('307', '34', 'Nias');
INSERT INTO `lokasi_kota` VALUES ('308', '34', 'Nias Barat');
INSERT INTO `lokasi_kota` VALUES ('309', '34', 'Nias Selatan');
INSERT INTO `lokasi_kota` VALUES ('310', '34', 'Nias Utara');
INSERT INTO `lokasi_kota` VALUES ('319', '34', 'Padang Lawas');
INSERT INTO `lokasi_kota` VALUES ('320', '34', 'Padang Lawas Utara');
INSERT INTO `lokasi_kota` VALUES ('323', '34', 'Padang Sidempuan');
INSERT INTO `lokasi_kota` VALUES ('325', '34', 'Pakpak Bharat');
INSERT INTO `lokasi_kota` VALUES ('353', '34', 'Pematang Siantar');
INSERT INTO `lokasi_kota` VALUES ('389', '34', 'Samosir');
INSERT INTO `lokasi_kota` VALUES ('404', '34', 'Serdang Bedagai');
INSERT INTO `lokasi_kota` VALUES ('407', '34', 'Sibolga');
INSERT INTO `lokasi_kota` VALUES ('413', '34', 'Simalungun');
INSERT INTO `lokasi_kota` VALUES ('459', '34', 'Tanjung Balai');
INSERT INTO `lokasi_kota` VALUES ('463', '34', 'Tapanuli Selatan');
INSERT INTO `lokasi_kota` VALUES ('464', '34', 'Tapanuli Tengah');
INSERT INTO `lokasi_kota` VALUES ('465', '34', 'Tapanuli Utara');
INSERT INTO `lokasi_kota` VALUES ('470', '34', 'Tebing Tinggi');
INSERT INTO `lokasi_kota` VALUES ('481', '34', 'Toba Samosir');

-- ----------------------------
-- Table structure for lokasi_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `lokasi_provinsi`;
CREATE TABLE `lokasi_provinsi` (
  `provinsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_provinsi` varchar(50) NOT NULL,
  PRIMARY KEY (`provinsi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lokasi_provinsi
-- ----------------------------
INSERT INTO `lokasi_provinsi` VALUES ('1', 'Bali');
INSERT INTO `lokasi_provinsi` VALUES ('2', 'Bangka Belitung');
INSERT INTO `lokasi_provinsi` VALUES ('3', 'Banten');
INSERT INTO `lokasi_provinsi` VALUES ('4', 'Bengkulu');
INSERT INTO `lokasi_provinsi` VALUES ('5', 'DI Yogyakarta');
INSERT INTO `lokasi_provinsi` VALUES ('6', 'DKI Jakarta');
INSERT INTO `lokasi_provinsi` VALUES ('7', 'Gorontalo');
INSERT INTO `lokasi_provinsi` VALUES ('8', 'Jambi');
INSERT INTO `lokasi_provinsi` VALUES ('9', 'Jawa Barat');
INSERT INTO `lokasi_provinsi` VALUES ('10', 'Jawa Tengah');
INSERT INTO `lokasi_provinsi` VALUES ('11', 'Jawa Timur');
INSERT INTO `lokasi_provinsi` VALUES ('12', 'Kalimantan Barat');
INSERT INTO `lokasi_provinsi` VALUES ('13', 'Kalimantan Selatan');
INSERT INTO `lokasi_provinsi` VALUES ('14', 'Kalimantan Tengah');
INSERT INTO `lokasi_provinsi` VALUES ('15', 'Kalimantan Timur');
INSERT INTO `lokasi_provinsi` VALUES ('16', 'Kalimantan Utara');
INSERT INTO `lokasi_provinsi` VALUES ('17', 'Kepulauan Riau');
INSERT INTO `lokasi_provinsi` VALUES ('18', 'Lampung');
INSERT INTO `lokasi_provinsi` VALUES ('19', 'Maluku');
INSERT INTO `lokasi_provinsi` VALUES ('20', 'Maluku Utara');
INSERT INTO `lokasi_provinsi` VALUES ('21', 'Nanggroe Aceh Darussalam (NAD)');
INSERT INTO `lokasi_provinsi` VALUES ('22', 'Nusa Tenggara Barat (NTB)');
INSERT INTO `lokasi_provinsi` VALUES ('23', 'Nusa Tenggara Timur (NTT)');
INSERT INTO `lokasi_provinsi` VALUES ('24', 'Papua');
INSERT INTO `lokasi_provinsi` VALUES ('25', 'Papua Barat');
INSERT INTO `lokasi_provinsi` VALUES ('26', 'Riau');
INSERT INTO `lokasi_provinsi` VALUES ('27', 'Sulawesi Barat');
INSERT INTO `lokasi_provinsi` VALUES ('28', 'Sulawesi Selatan');
INSERT INTO `lokasi_provinsi` VALUES ('29', 'Sulawesi Tengah');
INSERT INTO `lokasi_provinsi` VALUES ('30', 'Sulawesi Tenggara');
INSERT INTO `lokasi_provinsi` VALUES ('31', 'Sulawesi Utara');
INSERT INTO `lokasi_provinsi` VALUES ('32', 'Sumatera Barat');
INSERT INTO `lokasi_provinsi` VALUES ('33', 'Sumatera Selatan');
INSERT INTO `lokasi_provinsi` VALUES ('34', 'Sumatera Utara');

-- ----------------------------
-- Table structure for pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `pelanggan`;
CREATE TABLE `pelanggan` (
  `pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pelanggan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `hp` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `kota` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`pelanggan_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelanggan
-- ----------------------------

-- ----------------------------
-- Table structure for pembelian
-- ----------------------------
DROP TABLE IF EXISTS `pembelian`;
CREATE TABLE `pembelian` (
  `pembelian_id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('daftar','selesai') NOT NULL,
  PRIMARY KEY (`pembelian_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembelian
-- ----------------------------

-- ----------------------------
-- Table structure for pembelian_detail
-- ----------------------------
DROP TABLE IF EXISTS `pembelian_detail`;
CREATE TABLE `pembelian_detail` (
  `pembelian_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pembelian_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`pembelian_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembelian_detail
-- ----------------------------
INSERT INTO `pembelian_detail` VALUES ('1', '1', '8', '2');
INSERT INTO `pembelian_detail` VALUES ('2', '1', '7', '1');

-- ----------------------------
-- Table structure for pembelian_temp
-- ----------------------------
DROP TABLE IF EXISTS `pembelian_temp`;
CREATE TABLE `pembelian_temp` (
  `pembelian_temp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`pembelian_temp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembelian_temp
-- ----------------------------

-- ----------------------------
-- Table structure for penjualan
-- ----------------------------
DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE `penjualan` (
  `penjualan_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice` varchar(40) NOT NULL,
  `pelanggan_id` int(11) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `tanggal` datetime NOT NULL,
  `total` bigint(20) NOT NULL,
  `kurir` varchar(20) NOT NULL,
  `pelayanan` varchar(50) NOT NULL,
  `ongkir` bigint(20) NOT NULL,
  `berat` int(11) NOT NULL,
  `status` enum('draft','konfirmasi','lunas') NOT NULL,
  `promo` bigint(20) NOT NULL,
  PRIMARY KEY (`penjualan_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penjualan
-- ----------------------------
INSERT INTO `penjualan` VALUES ('1', '1463764026', '1', '152', 'Jalan Padang IV', '2016-05-21 00:07:06', '389000', 'jne', 'OKE', '15000', '50', 'lunas', '0');
INSERT INTO `penjualan` VALUES ('2', '1463832706', '1', '48', 'Jalan Padang IV', '2016-05-21 19:11:46', '400000', 'jne', 'OKE', '17000', '30', 'lunas', '0');
INSERT INTO `penjualan` VALUES ('3', '1466109503', '2', '93', 'Alamat', '2016-06-17 03:38:23', '400000', 'jne', 'CTCYES', '13000', '30', 'lunas', '30000');
INSERT INTO `penjualan` VALUES ('4', '1466671419', '2', '93', 'Alamat', '2016-06-23 15:43:39', '170000', 'pos', 'Surat Kilat Khusus', '9000', '30', 'konfirmasi', '30000');
INSERT INTO `penjualan` VALUES ('5', '1466930733', '2', '93', 'Alamat', '2016-06-26 15:45:33', '359000', 'jne', 'CTCYES', '13000', '50', 'konfirmasi', '30000');
INSERT INTO `penjualan` VALUES ('6', '1468425819', '2', '93', 'Alamat', '2016-07-13 23:03:39', '170000', 'jne', 'CTC', '12000', '30', 'konfirmasi', '30000');
INSERT INTO `penjualan` VALUES ('11', '1485916152', '2', '93', 'Alamat', '2017-02-01 09:29:12', '338000', 'tiki', 'ONS', '16000', '30', 'lunas', '0');

-- ----------------------------
-- Table structure for penjualan_detail
-- ----------------------------
DROP TABLE IF EXISTS `penjualan_detail`;
CREATE TABLE `penjualan_detail` (
  `penjualan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `penjualan_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `subtotal` bigint(20) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`penjualan_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penjualan_detail
-- ----------------------------
INSERT INTO `penjualan_detail` VALUES ('1', '1', '10', '1', '200000', '200000', '');
INSERT INTO `penjualan_detail` VALUES ('2', '1', '9', '1', '189000', '189000', '');
INSERT INTO `penjualan_detail` VALUES ('3', '2', '10', '2', '200000', '400000', '');
INSERT INTO `penjualan_detail` VALUES ('4', '3', '10', '2', '200000', '400000', '');
INSERT INTO `penjualan_detail` VALUES ('5', '4', '10', '1', '170000', '170000', '');
INSERT INTO `penjualan_detail` VALUES ('6', '5', '10', '1', '170000', '170000', '');
INSERT INTO `penjualan_detail` VALUES ('7', '5', '9', '1', '189000', '189000', '');
INSERT INTO `penjualan_detail` VALUES ('8', '6', '10', '1', '170000', '170000', 'esfsdfsdf');
INSERT INTO `penjualan_detail` VALUES ('9', '9', '7', '1', '139000', '139000', 'abcd');
INSERT INTO `penjualan_detail` VALUES ('10', '10', '7', '1', '139000', '139000', '');

-- ----------------------------
-- Table structure for penjualan_konfirmasi
-- ----------------------------
DROP TABLE IF EXISTS `penjualan_konfirmasi`;
CREATE TABLE `penjualan_konfirmasi` (
  `konfirmasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice` varchar(100) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `pemilik` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `tanggal_transfer` date NOT NULL,
  `bukti_transfer` varchar(100) NOT NULL,
  `penjualan_id` int(11) NOT NULL,
  PRIMARY KEY (`konfirmasi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penjualan_konfirmasi
-- ----------------------------

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_produk` varchar(30) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `merek_id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` double NOT NULL,
  `jumlah_lihat` int(11) NOT NULL,
  `jumlah_beli` int(11) NOT NULL,
  PRIMARY KEY (`produk_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES ('7', 'A0001', 'Sepatu Ibu-ibu', '3', '4', '6', '<p>Deskripsi Sepatu Ibu-ibu</p>\r\n', '169000', '30', '0', '3');
INSERT INTO `produk` VALUES ('8', 'A0002', 'Sepatu Anak-anak', '3', '4', '5', '<p>Sepatu anak-anak</p>\r\n', '89900', '30', '0', '4');
INSERT INTO `produk` VALUES ('9', 'A0003', 'Sepatu Balet Remaja', '3', '4', '5', '<p>Deskripsi Sepatu Balet Remaja</p>\r\n', '189000', '20', '0', '1');
INSERT INTO `produk` VALUES ('10', 'A0004', 'Sepatu Kets Remaja', '3', '4', '5', '<p>Deskripsi Sepatu Kets Remaja</p>\r\n', '200000', '30', '0', '3');

-- ----------------------------
-- Table structure for produk_kategori
-- ----------------------------
DROP TABLE IF EXISTS `produk_kategori`;
CREATE TABLE `produk_kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk_kategori
-- ----------------------------
INSERT INTO `produk_kategori` VALUES ('5', 'Buah');
INSERT INTO `produk_kategori` VALUES ('6', 'Sayur');

-- ----------------------------
-- Table structure for produk_merek
-- ----------------------------
DROP TABLE IF EXISTS `produk_merek`;
CREATE TABLE `produk_merek` (
  `merek_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_merek` varchar(50) NOT NULL,
  PRIMARY KEY (`merek_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk_merek
-- ----------------------------

-- ----------------------------
-- Table structure for produk_photo
-- ----------------------------
DROP TABLE IF EXISTS `produk_photo`;
CREATE TABLE `produk_photo` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  PRIMARY KEY (`photo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk_photo
-- ----------------------------
INSERT INTO `produk_photo` VALUES ('9', '1', 'produk_1-3.jpg');
INSERT INTO `produk_photo` VALUES ('8', '1', 'produk_1-2.jpg');
INSERT INTO `produk_photo` VALUES ('7', '1', 'produk_1-1.jpg');
INSERT INTO `produk_photo` VALUES ('10', '1', 'produk_1-4.jpg');
INSERT INTO `produk_photo` VALUES ('12', '2', 'produk_2-1.jpg');
INSERT INTO `produk_photo` VALUES ('17', '5', 'produk_5-1.jpg');
INSERT INTO `produk_photo` VALUES ('16', '4', 'produk_4-1.jpg');
INSERT INTO `produk_photo` VALUES ('18', '6', 'produk_6-1.jpg');
INSERT INTO `produk_photo` VALUES ('27', '8', 'produk_8-1.jpg');
INSERT INTO `produk_photo` VALUES ('22', '3', 'produk_3-1.jpg');
INSERT INTO `produk_photo` VALUES ('26', '7', 'produk_7-1.jpg');
INSERT INTO `produk_photo` VALUES ('28', '9', 'produk_9-1.jpg');
INSERT INTO `produk_photo` VALUES ('29', '10', 'produk_10-1.jpg');

-- ----------------------------
-- Table structure for produk_stok
-- ----------------------------
DROP TABLE IF EXISTS `produk_stok`;
CREATE TABLE `produk_stok` (
  `stok_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) NOT NULL,
  `toko_id` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_mutasi` int(11) NOT NULL,
  `stok_jual` int(11) NOT NULL,
  PRIMARY KEY (`stok_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk_stok
-- ----------------------------
INSERT INTO `produk_stok` VALUES ('9', '7', '1', '41', '1', '2');
INSERT INTO `produk_stok` VALUES ('10', '8', '1', '32', '0', '0');
INSERT INTO `produk_stok` VALUES ('11', '9', '1', '5', '0', '1');
INSERT INTO `produk_stok` VALUES ('12', '10', '1', '60', '0', '3');
INSERT INTO `produk_stok` VALUES ('14', '7', '3', '1', '0', '0');

-- ----------------------------
-- Table structure for produk_ukuran
-- ----------------------------
DROP TABLE IF EXISTS `produk_ukuran`;
CREATE TABLE `produk_ukuran` (
  `ukuran_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ukuran` varchar(20) NOT NULL,
  PRIMARY KEY (`ukuran_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk_ukuran
-- ----------------------------
INSERT INTO `produk_ukuran` VALUES ('7', '32');
INSERT INTO `produk_ukuran` VALUES ('6', '31');
INSERT INTO `produk_ukuran` VALUES ('5', '30');
INSERT INTO `produk_ukuran` VALUES ('8', '34');
INSERT INTO `produk_ukuran` VALUES ('9', '36');
INSERT INTO `produk_ukuran` VALUES ('10', '40');

-- ----------------------------
-- Table structure for promo
-- ----------------------------
DROP TABLE IF EXISTS `promo`;
CREATE TABLE `promo` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `mulai` date NOT NULL,
  `selesai` date NOT NULL,
  `banner_gambar` text NOT NULL,
  PRIMARY KEY (`promo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of promo
-- ----------------------------

-- ----------------------------
-- Table structure for promo_data
-- ----------------------------
DROP TABLE IF EXISTS `promo_data`;
CREATE TABLE `promo_data` (
  `promo_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  PRIMARY KEY (`promo_data_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of promo_data
-- ----------------------------
INSERT INTO `promo_data` VALUES ('1', '1', '7');
INSERT INTO `promo_data` VALUES ('2', '1', '8');
INSERT INTO `promo_data` VALUES ('3', '1', '10');

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(30) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES ('3', 'Supplier Sepatu', 'Alamat', '123123', '12');

-- ----------------------------
-- Table structure for toko
-- ----------------------------
DROP TABLE IF EXISTS `toko`;
CREATE TABLE `toko` (
  `toko_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_toko` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telepon` varchar(30) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `tipe` enum('pusat','cabang') NOT NULL,
  PRIMARY KEY (`toko_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of toko
-- ----------------------------
INSERT INTO `toko` VALUES ('1', 'Ivo Clothes Pusat', 'Jalan Gunung pangilun', '12345', '318', 'pusat');
INSERT INTO `toko` VALUES ('3', 'Ivo Bukittinggi', 'Jalan Bukittinggi', '55555', '93', 'cabang');

-- ----------------------------
-- Table structure for toko_mutasi
-- ----------------------------
DROP TABLE IF EXISTS `toko_mutasi`;
CREATE TABLE `toko_mutasi` (
  `mutasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  `toko_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`mutasi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of toko_mutasi
-- ----------------------------
INSERT INTO `toko_mutasi` VALUES ('1', '2016-05-12 03:13:53', '3', '4', 'keterangan');
INSERT INTO `toko_mutasi` VALUES ('5', '2016-06-23 11:53:56', '3', '4', '');

-- ----------------------------
-- Table structure for toko_mutasi_detail
-- ----------------------------
DROP TABLE IF EXISTS `toko_mutasi_detail`;
CREATE TABLE `toko_mutasi_detail` (
  `mutasi_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `mutasi_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `ukuran_id` int(11) NOT NULL,
  `warna_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`mutasi_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of toko_mutasi_detail
-- ----------------------------
INSERT INTO `toko_mutasi_detail` VALUES ('1', '1', '2', '2', '1', '2');
INSERT INTO `toko_mutasi_detail` VALUES ('2', '5', '7', '0', '0', '1');

-- ----------------------------
-- Table structure for userlogin
-- ----------------------------
DROP TABLE IF EXISTS `userlogin`;
CREATE TABLE `userlogin` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `akses` enum('admin','member','op','bos','supplier') NOT NULL,
  `photo` varchar(100) NOT NULL,
  `status` enum('aktif','banned') NOT NULL,
  `terakhir_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `toko_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of userlogin
-- ----------------------------
INSERT INTO `userlogin` VALUES ('1', 'Administrator', 'admin', 'e00cf25ad42683b3df678c61f42c6bda', 'admin', 'ava-c4ca4238a0b923820dcc509a6f75849b.jpg', 'aktif', '2017-02-01 10:58:15', null);
INSERT INTO `userlogin` VALUES ('4', 'Operator', 'op', '37832cda757792aef82ce5e21f542006', 'op', '', 'aktif', '2017-02-01 09:59:57', null);
INSERT INTO `userlogin` VALUES ('5', 'Pimpinan', 'bos', 'af67b0178b2677ef9f308bdc7e2f1670', 'bos', '', 'aktif', '2017-01-31 13:45:50', null);
INSERT INTO `userlogin` VALUES ('9', 'Heru', 'heru', 'a648ab9a3e32c5f3f6e9ddbd41c0530f', 'member', '', 'aktif', '2017-02-01 09:32:38', null);
INSERT INTO `userlogin` VALUES ('8', 'Esti Oneng', 'jkt', 'a936a0058e0963ff14134e1d47dc8107', 'member', '', 'aktif', '2016-05-21 19:08:30', null);
INSERT INTO `userlogin` VALUES ('12', 'Supplier Akun 1', 's1', '8ddf878039b70767c4a5bcf4f0c4f65e', 'supplier', '', 'aktif', '2016-07-13 23:09:10', null);
